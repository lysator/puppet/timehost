# Class for adding a puppet service to the system.
class timehost::puppet_agent {
  file { '/etc/sv/puppet':
    ensure  => directory,
    source  => 'puppet:///modules/timehost/services/puppet',
    recurse => false,
    owner   => 'root',
    group   => 'root',
    notify  => Service['puppet'],
  }

  file { '/etc/sv/puppet/run':
    ensure  => directory,
    source  => 'puppet:///modules/timehost/services/puppet/run',
    recurse => false,
    owner   => 'root',
    group   => 'root',
    mode    => '0774',
    notify  => Service['puppet'],
  }

  service { 'puppet':
    ensure => running,
    enable => true,
  }
}
