# Install fcron.
class timehost::fcron {
  package { 'fcron':
    ensure => present,
    notify => Service['puppet']
  }

  service { 'fcron':
    ensure => running,
    enable => true,
  }
}
