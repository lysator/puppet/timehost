# NTP server configuration.
class timehost::ntp_server {
  package {
    [
      'chrony',
      'gpsd',
    ]:
      ensure => installed,
  }

  service { 'gpsd':
    ensure => running,
    enable => true,
  }

  file { '/etc/sv/gpsd/conf':
    ensure => present,
    source => 'puppet:///modules/timehost/gpsd/conf',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Service['gpsd'],
  }

  service { 'chronyd':
    ensure => running,
    enable => true,
    notify => Service['gpsd'],
  }

  file { '/etc/chrony.conf':
    ensure => present,
    source => 'puppet:///modules/timehost/chrony/chrony.conf',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Service['chronyd'],
  }
}
