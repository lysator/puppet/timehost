# Class for only logging to Lysator's log server.
# Used to reduce wear to local storage.
class timehost::remote_logging {
  package { 'rsyslog':
    ensure => present,
  }

  file {'/etc/rsyslog.conf':
    ensure => present,
    source => 'puppet:///modules/timehost/rsyslog.conf',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Service['rsyslogd'],
  }

  service { 'rsyslogd':
    ensure => running,
    enable => true,
  }
}
