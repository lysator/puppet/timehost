# Class for adding DNS configuration to the system
class timehost::dns_config {
  file { '/etc/resolv.conf':
    ensure => file,
    source => 'puppet:///modules/timehost/resolv.conf',
    owner  => 'root',
    group  => 'root',
  }
}

