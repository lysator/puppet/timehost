# Install boot configuration to be able to use the GPS module with the
# Raspberry Pi 3 B+.
class timehost::rpi3_boot_config {
  file { '/boot/cmdline.txt':
    ensure => present,
    source => 'puppet:///modules/timehost/bootfiles/cmdline.txt',
    owner  => 'root',
    group  => 'root',
  }

  file { '/boot/config.txt':
    ensure => present,
    source => 'puppet:///modules/timehost/bootfiles/config.txt',
    owner  => 'root',
    group  => 'root',
  }
}
