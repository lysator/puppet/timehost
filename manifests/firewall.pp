# Install an nftable firewall.
class timehost::firewall {
  package { 'nftables':
    ensure => present,
  }

  service { 'nftables':
    ensure => running,
    enable => true,
  }

  file { '/etc/nftables.conf':
    ensure => present,
    source => 'puppet:///modules/timehost/nftables.conf',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    notify => Service['nftables'],
  }
}
